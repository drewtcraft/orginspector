import React from 'react';
import renderer from 'react-test-renderer';
import Modal from './index';

it('renders correctly', () => {
	const props = {
		text: 'xyz',
		component: () => <div>hello</div>,
	}
	const tree = renderer
		.create(<Modal {...props}  />)
		.toJSON();

	expect(tree).toMatchSnapshot();
});
