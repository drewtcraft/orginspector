import React from 'react';
import './style.css';

export default function Modal(props) {
	return <div className='modal-bg'>
		<div className='modal'>
			{ props.text }
			{ props.component }
			<button 
				className='dismiss-modal'
				onClick={ props.dismissModal }
			>
				dismiss
			</button>
		</div>
	</div>
}
