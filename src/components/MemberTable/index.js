import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { GET_MEMBER_URL } from '../../shared/constants';

function MemberTableRow(props) {
	return <tr>
		<td>{ props.name }</td>
		<td>{ props.title }</td>
		<td>{ props.phone_number }</td>
	</tr>
}

export default function MemberTable({ id }) {
	if (!id) throw new Error('property "id" required');

	const [ members, setMembers ] = useState([]);

	useEffect(() => {
		function filterMembers(_members) {
			console.log('id', id);
			console.log('members', _members);
			return _members.filter(m => {
				return m[`organization_id`] === `organization_id ${id}`;
			});
		}

		axios.get(GET_MEMBER_URL)
			.then(({ data }) => data)
			.then(filterMembers)
			.then(a => {
				console.log('members2', a);
				return a;
			})
			.then(setMembers)
			.catch(err => {
				console.error(err);
				window.alert(`Problem getting members: ${err.message}`);
			});
	}, [ id ]);

	const rows = members.map(o => {
		return <MemberTableRow { ...o } />
	});

	return <table className='member-table'>
		<thead>
			<tr>
				<td>NAME</td>
				<td>TITLE</td>
				<td>PHONE</td>
			</tr>
		</thead>
		<tbody>
			{ rows }
		</tbody>
	</table>;
}
