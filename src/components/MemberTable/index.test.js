import React from 'react';
import renderer from 'react-test-renderer';
import MemberTable from './index';

it('renders correctly', () => {
	const tree = renderer
		.create(<MemberTable id={1} />)
		.toJSON();

	expect(tree).toMatchSnapshot();
});
