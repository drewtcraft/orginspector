import React from 'react';
import './style.css';

function OrgTableRow(props) {
	return <tr onClick={ props.handleClick }>
		<td>{ props.name }</td>
		<td>{ props.is_public ? 'public' : 'private' }</td>
		<td>{ props.headcount }</td>
	</tr>
}

export default function OrgTable(props) {
	const rows = props.orgs.map(o => {
		return <OrgTableRow
			{ ...o }
			handleClick={ () => props.handleOrgClick(o) }
		/>
	})
	return <table className='organization-table'>
		<thead>
			<tr>
				<td>NAME</td>
				<td>PUBLIC/PRIVATE</td>
				<td>HEADCOUNT</td>
			</tr>
		</thead>
		<tbody>
			{ rows }
		</tbody>
	</table>;
}
