import React from 'react';
import renderer from 'react-test-renderer';
import OrgTable from './index';

it('renders correctly', () => {
	const tree = renderer
		.create(<OrgTable orgs={[]}  />)
		.toJSON();

	expect(tree).toMatchSnapshot();
});
