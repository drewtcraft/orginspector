import React from 'react';
import renderer from 'react-test-renderer';
import OrgInspector from './index';

it('renders correctly', () => {
	const props = {
		name: 'orgorg',
		address_1: '123 fake st',
		city: 'ny',
		state: 'ny',
		zip: 10001,
		id: 1,
		headcount: 10,
	}
	const tree = renderer
		.create(<OrgInspector {...props}  />)
		.toJSON();

	expect(tree).toMatchSnapshot();
});
