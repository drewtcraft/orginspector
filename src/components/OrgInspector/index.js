import React from 'react';
import MemberTable from '../MemberTable';
import './style.css';

function LabeledField(props) {
	return <div className='labeled-field'>
		<label>{ props.label }</label>
		<div>{ props.value }</div>
	</div>;
}

export default function OrgInspector(props) {
	return <div className='org-inspector'>
		<LabeledField
			label='NAME'
			value={ props.name }
		/>
		<LabeledField
			label='ADDRESS'
			value={ `${props.address_1} ${props.city}, ${props.state} ${props.zip}`}
		/>
		<LabeledField
			label='HEADCOUNT'
			value={ props.headcount }
		/>
		<LabeledField
			label='PUBLIC/PRIVATE'
			value={ props.is_public ? 'PUBLIC' : 'PRIVATE' }
		/>
		<label>MEMBERS</label>
		<MemberTable id={ props.id } />
	</div>
}
