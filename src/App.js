import React, { useState, useEffect } from 'react';
import OrgTable from './components/OrgTable';
import OrgInspector from './components/OrgInspector';
import Modal from './components/Modal';
import * as constants from './shared/constants';
import axios from 'axios';
import './App.css';

const modalDefault = {
	text: null,
	component: null,
}

function App() {
	const [ orgs, setOrgs ] = useState([]);
	const [ showModal, setShowModal ] = useState(false);
	const [ modalConfig, setModalConfig ] = useState({ ...modalDefault });

	function handleOrgClick(org) {
		axios.get(constants.GET_MEMBER_URL)
			.then(({ data }) => {
				const config = {
					component: <OrgInspector { ...org } />,
				};
				setModalConfig(config);
				setShowModal(true)
			})
	}

	const dismissModal = () => setShowModal(false);

	useEffect(() => {
		axios.get(constants.GET_ORG_URL)
			.then(({ data }) => {
				setOrgs(data);
			})
			.catch(err => {
				console.error(err);
				window.alert(`Problem getting organizations: ${err.message}`);
			});
	}, [])

	let modal;
	if (showModal) {
		modal = <Modal 
			{ ...modalConfig } 
			dismissModal={ dismissModal }
		/>;
	}

	return <div className="App">
		{ modal }
		<h1>Org Inspector</h1>
		<OrgTable 
			orgs={ orgs } 
			handleOrgClick={ handleOrgClick }
		/>
	</div>
}

export default App;
